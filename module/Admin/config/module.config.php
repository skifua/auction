<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [

    // Doctrine config
//    'doctrine' => [
//        'driver' => [
//            __NAMESPACE__ . '_driver' => [
//                'class' =>  AnnotationDriver::class,
//                'cache' => 'array',
//                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity']
//            ],
//            'orm_default' => [                'drivers' => [
//                __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
//            ]
//            ]
//        ]
//    ],
    'controllers' => [
        'factories' => [
            Controller\GoodsAdminController::class => Controller\GoodsAdminControllerFactory::class,
            Controller\TypeAdminController::class => Controller\TypeAdminControllerFactory::class,
            Controller\UserAdminController::class => Controller\UserAdminControllerFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'admin-goods' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/admin/goods[/:action][/:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\GoodsAdminController::class,
                        'action'     => 'products',
                    ],
                ],
            ],

            'admin-type' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/admin/type[/:action][/:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\TypeAdminController::class,
                        'action'     => 'types',
                    ],
                ],
            ],

            'admin-user' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/admin/user[/:action][/:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\UserAdminController::class,
                        'action'     => 'users',
                    ],
                ],
            ],
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../../Application/view/layout/layout.phtml',
            'error/404'               => __DIR__ . '/../../Application/view/error/404.phtml',
            'error/index'             => __DIR__ . '/../../Application/view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
