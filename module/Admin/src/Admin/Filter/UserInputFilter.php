<?php


namespace Admin\Filter;


use Users\Filter\RegistrationInputFilter;

class UserInputFilter extends RegistrationInputFilter
{
    public function __construct()
    {
        parent::__construct();

        $this->add([
            'name' => 'id',
            'required' => false,
        ]);

        $this->add([
            'name' => 'state',
            'required' => false,
        ]);

        $this->add([
            'name' => 'password',
            'allow_empty' => true,
        ]);

        $this->add([
            'name' => 'repeat_password',
            'allow_empty' => true,
        ]);
    }
}