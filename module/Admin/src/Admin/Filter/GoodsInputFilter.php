<?php


namespace Admin\Filter;


use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;

class GoodsInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'type'     => FileInput::class,
            'name'     => 'file',
            'required' => false,
            'validators' => [
                ['name'    => 'FileUploadFile'],
                ['name'    => 'FileIsImage'],
                [
                    'name'    => 'FileImageSize',
                    'options' => [
                        'minWidth'  => 128,
                        'minHeight' => 128,
                        'maxWidth'  => 4096,
                        'maxHeight' => 4096
                    ]
                ],
            ],
            'filters'  => [
                [
                        'name' => 'FileRenameUpload',
                    'options' => [
                        'target'=>'./img/images/images/upload',
                        'useUploadName'=>true,
                        'useUploadExtension'=>true,
                        'overwrite'=>true,
                        'randomize'=>false
                    ]
                ]
            ],
        ]);

        $this->add( [
            'name' => 'name',
            'required' => true,
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 2,
                        'max' => 50,
                    ],
                ],
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],

        ]);

        $this->add( [
            'name' => 'price',
            'required' => false,
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 2,
                        'max' => 50,
                    ],
                ],
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],

        ]);

        $this->add( [
            'name' => 'startPrice',
            'required' => true,
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 2,
                        'max' => 50,
                    ],
                ],
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],

        ]);

        $this->add( [
            'name' => 'stepPrice',
            'required' => true,
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 50,
                    ],
                ],
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],

        ]);

        $this->add([
            'name' => 'dateStart',
            'required' => true,
        ]);

        $this->add([
            'name' => 'dateStop',
            'required' => true,
        ]);

        $this->add([
            'name' => 'photo',
            'required' => false,
        ]);

        $this->add([
            'name' => 'id',
            'required' => false,
        ]);

        $this->add([
            'name' => 'typeId',
            'required' => false,
        ]);

        $this->add([
            'name' => 'dynamic',
            'required' => false,
        ]);

        $this->add([
            'name' => 'descriptions',
            'required' => false,
        ]);

        $this->add([
            'name' => 'csrf',
            'required' => false,
        ]);

    }
}