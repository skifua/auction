<?php


namespace Admin\Model;


class GoodsAdminModel
{
    const DEFAULT_TIME_WORK_AUCTION = 10;
    const MESSAGE_SUCCESS_EDIT_GOODS = 'Data Lot updated';
    /**
     * @var object EntityManager
     */
    protected $em;

    /**
     * @var object AuthStorage
     */
    protected $authStorage;

    public function __construct($em, $authStorage)
    {
        $this->em = $em;
        $this->authStorage = $authStorage->read();
    }

}
