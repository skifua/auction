<?php

namespace Admin\Model;

use Application\Model\HashModel;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use Users\Entity\User;

class UserAdminModel
{
    const USERS_IN_ADMIN_LIST = 10;
    const MESSAGE_SUCCESS_EDIT_USER = 'Data User updated';

    /**
     * @var object EntityManager
     */
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * @return Paginator
     */
    public function fetchAll()
    {
        $result = $this->em->getRepository(User::class)
            ->findAll();
        $paginator = new Paginator(new ArrayAdapter($result));

        return $paginator;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserById($id)
    {
        $result = $this->em->getRepository(User::class)
            ->find($id);
        return $result;
    }

    /**
     * @param $user
     * @return bool
     */
    public function update($user)
    {

        $userDb = $this->em->getRepository(User::class)
            ->find($user->getId());
        if (!$userDb) {
            return false;
        }

        $userDb->setOptions($this->cleanArray($user->getArrayCopy()));
        if ($user->getState() == '') {
            $userDb->setState(null);
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param $user
     * @param $password
     * @return mixed
     */
    public function setPassword($user, $password)
    {
        if ($password == '' || $password == null) {
            $user->setPassword(null);
            return $user;
        }
        $hashModel = new HashModel();
        $user->setPassword($hashModel->hash($password));
        return $user;
    }

    /**
     * @param $array
     * @return array
     */
    protected function cleanArray($array)
    {
        $result = [];
        foreach($array as $key => $value) {
            if ($value && $value != '') {
                $result[$key] = $value;
            }
        }
        return $result;
    }



}