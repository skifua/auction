<?php


namespace Admin\Model;


use Admin\Exception\ImageException;

class ImagesModel
{
    const UPLOAD_IMAGE_MIN_SIZE = '600';
    const UPLOAD_IMAGE_MAX_SIZE = '80000000';
    const UPLOAD_IMAGE_PATH = 'public/img/images/images';
    const UPLOAD_IMAGE_PATH_VIEW = '/img/images/images';
    const ALL_PATH_TO_SITE = '/var/www/html/';
    const IMAGE_PATH_DEFAULT = 'default';
    const CUSTOMER_ID = 'auction';
    const IMAGE_PATH_MEDIUM = 'medium';
    const IMAGE_PATH_SMALL = 'small';
    const UPLOAD_SIZE_MEDIUM_WIDTH = '700';
    const UPLOAD_SIZE_MEDIUM_HEIGHT = '400';
    const UPLOAD_SIZE_SMALL_HEIGHT = '200';
    const UPLOAD_SIZE_SMALL_WIDTH = '350';
    const MESSAGE_SUCCESS_EDIT_IMAGE = 'Image success saved';

    /**
     * @return null|string
     */
    public function upload() {
        $fileName = null;
        $adapter = $this->getHttpAdapter();

        foreach($adapter->getFileInfo() as $file => $info) {
            if ($adapter->isValid($file)) {
                $name   = $adapter->getFileName($file);
                $fileName = self::CUSTOMER_ID . '_' . time() . '_' . $info['name'];

                $adapter->addFilter(
                    new \Zend\Filter\File\Rename(array('target' => $this->makeDefaultName($fileName),
                        'overwrite' => true)),
                    null, $file
                );
                if ($adapter->receive($file)) {

                    $this->makeImageMediumDefault($fileName);
                    $this->makeImageSmallDefault($fileName);
                }
            }
        }
        return $fileName;
    }

    /**
     * @param $fileName
     * @param $size
     * @return bool
     * @throws ImageException
     */
    public function resizeImage($fileName, $size)
    {
        $x1 = $size['x1'];
        $y1 = $size['y1'];
        $width = $size['w'];
        $height = $size['h'];

        self::deleteCropImages($fileName);

        if (!$this->makeImageMediumByPoint($fileName, $width, $height, $x1, $y1)) {
            return false;
        }

        if (!$this->makeImageSmallByPoint($fileName, $width, $height, $x1, $y1)) {
            return false;
        }
        return true;
    }


    /**
     * @return \Zend\File\Transfer\Adapter\Http
     */
    public function getHttpAdapter()
    {
        $adapter = new \Zend\File\Transfer\Adapter\Http();
        $adapter->addValidator('Size', false, ['min' => self::UPLOAD_IMAGE_MIN_SIZE, 'max' => self::UPLOAD_IMAGE_MAX_SIZE]);
        $adapter->addValidator('MimeType', true, ['image/jpeg', 'image/png', 'image/gif']);
        $adapter->addValidator('Count', false, ['min' => 1, 'max' => 1]);
        $adapter->addValidator('IsImage', false, 'png, jpeg, gif');
        $adapter->setDestination(self::UPLOAD_IMAGE_PATH . '/' . self::IMAGE_PATH_DEFAULT);

        return $adapter;
    }

    /**
     * @param $fileName
     * @return string
     */
    public function makeDefaultName($fileName) {
        return self::UPLOAD_IMAGE_PATH . '/' . self::IMAGE_PATH_DEFAULT . '/' . $fileName;
    }

    /**
     * @param $fileName
     * @return string
     */
    public function makeMediumName($fileName)
    {
        return self::UPLOAD_IMAGE_PATH . '/' . self::IMAGE_PATH_MEDIUM . '/' . $fileName;
    }

    /**
     * @param $fileName
     * @return string
     */
    public function makeSmallName($fileName)
    {
        return self::UPLOAD_IMAGE_PATH . '/' . self::IMAGE_PATH_SMALL . '/' . $fileName;
    }

    /**
     * @param $fileName
     */
    public function makeImageMediumDefault($fileName)
    {
        $image = new \Imagick();

        $image->readImage($this->makeDefaultName($fileName));
        $imageprops = $image->getImageGeometry();
        if ($imageprops['width'] > self::UPLOAD_SIZE_MEDIUM_WIDTH || $imageprops['height'] > self::UPLOAD_SIZE_MEDIUM_HEIGHT) {
            $image->resizeImage(self::UPLOAD_SIZE_MEDIUM_WIDTH, self::UPLOAD_SIZE_MEDIUM_HEIGHT, $image::FILTER_LANCZOS, 0.9, true);
        }
        $imageprops = $image->getImageGeometry();
        if ($imageprops['width'] < self::UPLOAD_SIZE_MEDIUM_WIDTH || $imageprops['height'] < self::UPLOAD_SIZE_MEDIUM_HEIGHT) {
            $width = (int) ((self::UPLOAD_SIZE_MEDIUM_WIDTH - $imageprops['width']) / 2 );
            $height = (int) ((self::UPLOAD_SIZE_MEDIUM_HEIGHT - $imageprops['height']) / 2);
            $image->borderImage('white', $width, $height);
        }
        if (!$image->writeImage($this->makeMediumName($fileName))) {
            throw new ImageException(' Error make Medium Image Default');
        }
        return true;
    }

    /**
     * @param $fileName
     */
    public function makeImageSmallDefault($fileName)
    {
        $image = new \Imagick();

        $image->readImage($this->makeDefaultName($fileName));
        $imageprops = $image->getImageGeometry();
        if ($imageprops['width'] > self::UPLOAD_SIZE_SMALL_WIDTH || $imageprops['height'] > self::UPLOAD_SIZE_SMALL_HEIGHT) {
            $image->resizeImage(self::UPLOAD_SIZE_SMALL_WIDTH, self::UPLOAD_SIZE_SMALL_HEIGHT, $image::FILTER_LANCZOS, 0.9, true);
            $imageprops = $image->getImageGeometry();
        }
        if ($imageprops['width'] < self::UPLOAD_SIZE_SMALL_WIDTH || $imageprops['height'] < self::UPLOAD_SIZE_SMALL_HEIGHT) {
            $width = (int) ((self::UPLOAD_SIZE_SMALL_WIDTH - $imageprops['width']) / 2 );
            $height = (int) ((self::UPLOAD_SIZE_SMALL_HEIGHT - $imageprops['height']) / 2);
            $image->borderImage('white', $width, $height);
        }
        if (!$image->writeImage($this->makeSmallName($fileName))) {
            throw new ImageException(' Error make Small Image Default');
        }
        return true;
    }

    /**
     * @param $fileName
     * @param $x1
     * @param $y1
     */
    public function makeImageSmallByPoint($fileName, $width, $height, $x1, $y1)
    {
        $image = new \Imagick();

        $image->readImage($this->makeDefaultName($fileName));
        $image->cropImage($width, $height, $x1, $y1);

        $imageprops = $image->getImageGeometry();
        if ($imageprops['width'] > self::UPLOAD_SIZE_SMALL_WIDTH || $imageprops['height'] > self::UPLOAD_SIZE_SMALL_HEIGHT) {
            $image->resizeImage(self::UPLOAD_SIZE_SMALL_WIDTH, self::UPLOAD_SIZE_SMALL_HEIGHT, $image::FILTER_LANCZOS, 0.9, true);
        }

        if (!$image->writeImage($this->makeSmallName($fileName))) {
            throw new ImageException(' Error make Small Image');
        }
        return true;
    }

    /**
     * @param $fileName
     * @param $x1
     * @param $y1
     */
    public function makeImageMediumByPoint($fileName, $width, $height, $x1, $y1)
    {
        $image = new \Imagick();

        $image->readImage($this->makeDefaultName($fileName));
        $image->cropImage($width, $height, $x1, $y1);

        $imageprops = $image->getImageGeometry();
        if ($imageprops['width'] > self::UPLOAD_SIZE_MEDIUM_WIDTH || $imageprops['height'] > self::UPLOAD_SIZE_MEDIUM_HEIGHT) {
            $image->resizeImage(self::UPLOAD_SIZE_MEDIUM_WIDTH, self::UPLOAD_SIZE_MEDIUM_HEIGHT, $image::FILTER_LANCZOS, 0.9, true);
        }

        if (!$image->writeImage($this->makeMediumName($fileName))) {
            throw new ImageException(' Error make Medium Image');
        }
        return true;
    }

    /**
     * @param $fileName
     */
    public static function deleteImages($fileName)
    {
        if (file_exists(self::ALL_PATH_TO_SITE . self::UPLOAD_IMAGE_PATH . '/' . self::IMAGE_PATH_DEFAULT . '/' . $fileName)) {
            if (!unlink(self::ALL_PATH_TO_SITE . self::UPLOAD_IMAGE_PATH . '/' . self::IMAGE_PATH_DEFAULT . '/' . $fileName)) {
                throw new ImageException('Error delete default image ' .$fileName);
            }
        }

        if (!self::deleteCropImages($fileName)) {
            return false;
        }
         return true;
    }

    /**
     * @param $fileName
     */
    public static function deleteCropImages($fileName)
    {
        if (file_exists(self::ALL_PATH_TO_SITE . self::UPLOAD_IMAGE_PATH . '/' . self::IMAGE_PATH_MEDIUM . '/' . $fileName)) {
            if (!unlink(self::ALL_PATH_TO_SITE . self::UPLOAD_IMAGE_PATH . '/' . self::IMAGE_PATH_MEDIUM . '/' . $fileName)) {
                throw new ImageException('Error delete medium image ' .$fileName);
            }
        }
        if (file_exists(self::ALL_PATH_TO_SITE . self::UPLOAD_IMAGE_PATH . '/' . self::IMAGE_PATH_SMALL . '/' . $fileName)) {
            if (!unlink(self::ALL_PATH_TO_SITE . self::UPLOAD_IMAGE_PATH . '/' . self::IMAGE_PATH_SMALL . '/' . $fileName)) {
                throw new ImageException('Error delete small image ' .$fileName);
            }
        }
        return true;
    }
}