<?php


namespace Admin\Form;

use Admin\Filter\TypeGoodsInputFilter;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;


class TypeForm extends Form
{
    public function __construct()
    {
        parent::__construct();

        $this->setAttribute('method', 'post');


        $this->add([
            'name' => 'id',
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => 'name',
            'type' => 'text',
            'attributes' => [
                'placeholder' => 'Name',
                'autocomplete' => 'off'
            ],
        ]);

        $this->add([
            'name' => 'descriptions',
            'type'  => Textarea::class,
            'attributes' => [
                'placeholder' => 'Description',
                'autocomplete' => 'off'
            ],
        ]);

        $this->add([
            'name' => 'submitType',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Save',
                'class' => 'btn btn-success btn-margin'
            ],
        ]);

        $this->setInputFilter(new TypeGoodsInputFilter());
    }
}