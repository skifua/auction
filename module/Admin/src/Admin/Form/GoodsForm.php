<?php


namespace Admin\Form;


use Admin\Filter\GoodsInputFilter;
use Admin\Model\GoodsAdminModel;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\DateTime;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class GoodsForm extends Form
{
    /**
     * @var object GoodsModel
     */
    protected $goodsModel;

    public function __construct($goodsModel)
    {
        $this->goodsModel = $goodsModel;

        parent::__construct();

        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add([
            'name' => 'id',
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => 'name',
            'type' => 'text',
            'attributes' => [
                'placeholder' => 'Name',
                'autocomplete' => 'off'
            ],
        ]);

        $this->add([
            'name' => 'descriptions',
            'type'  => Textarea::class,
            'attributes' => [
                'placeholder' => 'Description',
                'autocomplete' => 'off'
            ],
        ]);


        $typesGoods = $this->goodsModel->getListTypesGoods();
        $this->add([
            'name' => 'typeId',
            'type' => Select::class,
            'options' => [
                'value_options' => $typesGoods
            ]
        ]);

        $this->add([
            'name' => 'price',
            'type' => 'text',
            'attributes' => [
                'placeholder' => 'Price',
                'autocomplete' => 'off',
                'disabled' => 'disabled',
            ],
        ]);

        $this->add([
            'name' => 'startPrice',
            'type' => 'text',
            'attributes' => [
                'placeholder' => 'Start Price',
                'autocomplete' => 'off'
            ],
        ]);

        $this->add([
            'name' => 'stepPrice',
            'type' => 'text',
            'attributes' => [
                'placeholder' => 'Step Price',
                'autocomplete' => 'off'
            ],
        ]);


        $defaultStart = new \DateTime();
        $this->add([
            'type' => DateTime::class,
            'name' => 'dateStart',
            'options' => [
                'label' => 'Date Start',
                'format' => 'Y-m-d H:i'
            ],
            'attributes' => [
                'min' => '2016-09-01 00:00',
                'max' => '2020-01-01 00:00',
                'value' => $defaultStart,
                'step' => '1', // minutes; default step interval is 1 min
            ]
        ]);

        $defaultStop = new \DateTime();
        $defaultStop->add(new \DateInterval('P' . GoodsAdminModel::DEFAULT_TIME_WORK_AUCTION . 'D'));
        $this->add([
            'type' => DateTime::class,
            'name' => 'dateStop',
            'options' => [
                'label' => 'Date Start',
                'format' => 'Y-m-d H:i'
            ],
            'attributes' => [
                'min' => '2016-09-01 00:00',
                'max' => '2020-01-01 00:00',
                'value' => $defaultStop,
                'step' => '1', // minutes; default step interval is 1 min
            ]
        ]);

        $this->add([
            'type' => Csrf::class,
            'name' => 'csrf',
        ]);

        $this->add([
            'type'  => 'file',
            'name' => 'file',
            'attributes' => [
                'id' => 'file'
            ],
            'options' => [
                'label' => 'Upload file',
            ],
        ]);

        $this->add([
            'name' => 'photo',
            'type' => Hidden::class,
        ]);

        $this->add([
            'type' => Checkbox::class,
            'name' => 'dynamic',
            'options' => [
                'label' => 'Dynamic',
                'use_hidden_element' => true,
                'checked_value' => true,
                'unchecked_value' => false
            ],
        ]);

        $this->add([
            'name' => 'submitGoods',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Save',
                'class' => 'btn btn-success btn-margin'
            ],
        ]);

        $this->setInputFilter(new GoodsInputFilter());
    }
}
