<?php


namespace Admin\Form;


use Zend\Form\Element\Hidden;
use Zend\Form\Form;

class ImageResizeForm extends Form
{
    public function __construct()
    {
        parent::__construct();

        $this->setAttribute('method', 'post');

        $this->add([
            'name' => 'x1',
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => 'y1',
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => 'w',
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => 'h',
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => 'submitImage',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Resize',
                'class' => 'btn btn-success btn-margin'
            ],
        ]);

    }

}