<?php


namespace Admin\Controller;


use Admin\Exception\TypeAdminException;
use Admin\Form\ConfirmForm;
use Admin\Form\TypeForm;
use Admin\Model\TypeAdminModel;
use Application\Controller\TemplateController;
use Goods\Entity\TypeGoods;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;


class TypeAdminController extends TemplateController
{
    /**
     * @var object EntityManager
     */
    protected $em;

    /**
     * @var object FlashMessenger
     */
    protected $fm;

    /**
     * @var object TypeAdminModel
     */
    protected $typeAdminModel;

    /**
     * @var object Form
     */
    protected $form;

    /**
     * @var array objects
     */
    protected $types;

    /**
     * @var object
     */
    protected $type;

    public function __construct(
        $em,
        $typeAdminModel
    )
    {
        $this->em = $em;
        $this->typeAdminModel = $typeAdminModel;
        $this->fm = $this->plugin(FlashMessenger::class);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function editAction()
    {
        $this->setViewModelsList([
            'form',
            'fm'
        ]);

        $this->setForm(new TypeForm());

        $request = $this->getRequest();
        if ($request->isPost()) {

            $this->setForm($this->getForm()->setData($request->getPost()));

            if (!$this->getForm()->isValid()) {
                return $this->getViewModel();
            }

            $types = new TypeGoods((array) $request->getPost());

            if ($this->typeAdminModel->update($types)) {
                $this->fm->addMessage(TypeAdminModel::MESSAGE_SUCCESS_EDIT_TYPE);
                return $this->redirect()->toRoute('admin-type', ['action' => 'edit', 'id' => $request->getPost('id')]);
            }
            return $this->redirect()->toRoute('admin-type', ['action' => 'types']);
        }

        if (!$idType = $this->params('id', null)) {
            return $this->redirect()->toRoute('admin-type', ['action' => 'types']);
        }

        if (!$type = $this->typeAdminModel->getTypeById($idType)) {
            return $this->redirect()->toRoute('admin-type', ['action' => 'types']);
        }

        $form = $this->getForm();

        $this->setForm($form->setData($type->getArrayCopy()));
        return $this->getViewModel();
    }

    /**
     * @return \Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function typesAction()
    {
        $this->setViewModelsList([
            'types'
        ]);

        $this->setTypes($this->typeAdminModel->fetchAll());

        return $this->getViewModel();
    }

    /**
     * @return \Zend\Http\Response
     * @throws TypeAdminException
     */
    public function deleteAction()
    {
        $this->setViewModelsList([
            'form',
            'type'
        ]);

        $request = $this->getRequest();
        if ($request->isPost()) {
            if(!$this->typeAdminModel->deleteById($request->getPost('id', null))) {
                throw new TypeAdminException('Error delete! Type Id №'. $request->getPost('id', 'No number') . ' does not exist');
            }
            return $this->redirect()->toRoute('admin-type', ['action' => 'types']);
        }

        if (!$idType = $this->params('id', null)) {
            return $this->redirect()->toRoute('admin-type', ['action' => 'types']);
        }

        $form = new ConfirmForm();
        $form->get('id')->setValue($idType);
        $this->setForm($form);

        $this->setType($this->typeAdminModel->getTypeById($idType));

        return $this->getViewModel();
    }

    /**
     * @return \Zend\Http\Response|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function createTypeAction()
    {
        $this->setViewModelsList([
            'form'
        ]);
        $this->setForm(new TypeForm());

        $request = $this->getRequest();
        if ($request->isPost()) {

            $this->setForm($this->getForm()->setData($request->getPost()));

            if (!$this->getForm()->isValid()) {
                return $this->getViewModel();
            }

            $type = new TypeGoods($request->getPost()->toArray());

            if ($newId = $this->typeAdminModel->save($type)) {
                $this->fm->addMessage(TypeAdminModel::MESSAGE_SUCCESS_EDIT_TYPE);
                return $this->redirect()->toRoute('admin-type', ['action' => 'edit', 'id' => $newId]);
            }
            return $this->redirect()->toRoute('admin-type', ['action' => 'types']);
        }

        return $this->getViewModel();
    }

    /**
     * @param $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return object
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param $types
     */
    public function setTypes($types)
    {
        $this->types = $types;
    }

    /**
     * @return object
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @return mixed|object
     */
    public function getFm()
    {
        return $this->fm;
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return object
     */
    public function getType()
    {
        return $this->type;
    }

}