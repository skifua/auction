<?php


namespace Admin\Controller;


use Admin\Form\GoodsForm;
use Admin\Model\GoodsAdminModel;
use Goods\Model\GoodsModel;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class GoodsAdminControllerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $em = $container->get('doctrine.entitymanager.orm_default');
        $goodsModel = $container->get(GoodsModel::class);
        $goodsAdminModel = $container->get(GoodsAdminModel::class);
        $authStorage = $container->get('Auth_storage');
        $goodsForm = $container->get(GoodsForm::class);
        return new GoodsAdminController(
            $em,
            $goodsModel,
            $goodsAdminModel,
            $authStorage,
            $goodsForm
        );
    }

}