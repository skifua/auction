<?php


namespace Admin\Controller;


use Admin\Model\UserAdminModel;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserAdminControllerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $em = $container->get('doctrine.entitymanager.orm_default');
        $userAdminModel = $container->get(UserAdminModel::class);

        return new UserAdminController(
            $em,
            $userAdminModel
        );
    }

}