<?php


namespace Admin\Controller;


use Admin\Form\UserForm;
use Admin\Model\UserAdminModel;
use Application\Controller\TemplateController;
use Users\Entity\User;
use Users\Model\UserModel;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;

class UserAdminController extends TemplateController
{
    /**
     * @var object EntityManager
     */
    protected $em;

    /**
     * @var object FlashMessenger
     */
    protected $fm;

    /**
     * @var object UserAdminModel
     */
    protected $userAdminModel;

    /**
     * @var object Paginator
     */
    protected $paginator;

    /**
     * @var object Form
     */
    protected $form;


    public function __construct(
        $em,
        $userAdminModel
    )
    {
        $this->em = $em;
        $this->userAdminModel = $userAdminModel;
        $this->fm = $this->plugin(FlashMessenger::class);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function usersAction()
    {
        $this->layout()->setVariable('active', 'users');
        $this->setViewModelsList([
            'paginator',
        ]);

        $paginator = $this->userAdminModel->fetchAll();
        $paginator->setCurrentPageNumber((int) $this->params('id', 1));
        $paginator->setItemCountPerPage(UserAdminModel::USERS_IN_ADMIN_LIST);
        $this->setPaginator($paginator);

        return $this->getViewModel();
    }

    public function editAction()
    {
        $this->setViewModelsList([
            'form',
            'fm'
        ]);

        $this->setForm(new UserForm());

        $request = $this->getRequest();
        if ($request->isPost()) {

            $this->setForm($this->getForm()->setData($request->getPost()));

            if (!$this->getForm()->isValid()) {
                return $this->getViewModel();
            }

            if ($request->getPost('password', null) != $request->getPost('repeat_password', null)) {
                $repeatPassword = $this->getForm()->get('repeat_password')->setMessages([UserModel::PASSWORDS_DO_NOT_MATH]);
                $this->setForm($this->getForm()->add($repeatPassword));
                return $this->getViewModel();
            }

            $user = new User((array) $request->getPost());

            $user = $this->userAdminModel->setPassword(
                $user,
                $request->getPost('password', null)
            );


            if ($this->userAdminModel->update($user)) {
                $this->fm->addMessage(UserAdminModel::MESSAGE_SUCCESS_EDIT_USER);
                return $this->redirect()->toRoute('admin-user', ['action' => 'edit', 'id' => $request->getPost('id')]);
            }
            return $this->redirect()->toRoute('admin-user', ['action' => 'users']);
        }

        if (!$idUser = $this->params('id', null)) {
            return $this->redirect()->toRoute('admin-user', ['action' => 'users']);
        }

        if (!$user = $this->userAdminModel->getUserById($idUser)) {
            return $this->redirect()->toRoute('admin-user', ['action' => 'users']);
        }

        $form = $this->getForm();

        $this->setForm($form->setData($user->getArrayCopy()));
        return $this->getViewModel();
    }

    /**
     * @param $paginator
     */
    protected function setPaginator($paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * @return mixed
     */
    protected function getPaginator()
    {
        return $this->paginator;
    }

    /**
     * @param $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return object
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @return mixed|object
     */
    public function getFm()
    {
        return $this->fm;
    }
}