<?php


namespace Admin\Controller;


use Admin\Exception\ImageException;
use Admin\Form\ImageResizeForm;
use Admin\Model\GoodsAdminModel;
use Admin\Model\ImagesModel;
use Application\Controller\TemplateController;
use Goods\Entity\Goods;
use Goods\Model\GoodsModel;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;

class GoodsAdminController extends TemplateController
{
    /**
     * @var object EntityManager
     */
    protected $em;

    /**
     * @var object FlashMessenger
     */
    protected $fm;

    /**
     * @var object GoodsModel
     */
    protected $goodsModel;

    /**
     * @var object GoodsAdminModel
     */
    protected $goodsAdminModel;

    /**
     * @var object AuthStorage
     */
    protected $authStorage;

    /**
     * @var object GoodsForm
     */
    protected $goodsForm;

    /**
     * @var object Paginator
     */
    protected $paginator;

    /**
     * @var string
     */
    protected $productTimeFormat;

    /**
     * @var string
     */
    protected $defaultPhoto;

    /**
     * @var string
     */
    protected $idActiveSpan;

    /**
     * @var string
     */
    protected $orderSorting;

    /**
     * @var string
     */
    protected $columnSorting;

    /**
     * @var string
     */
    protected $pathPhoto;

    /**
     * @var object Form
     */
    protected $form;

    /**
     * @var int
     */
    protected $idProduct;

    /**
     * @param $em
     * @param $goodsModel
     * @param $goodsAdminModel
     * @param $authStorage
     * @param $goodsForm
     */
    public function __construct(
        $em,
        $goodsModel,
        $goodsAdminModel,
        $authStorage,
        $goodsForm
    )
    {
        $this->em = $em;
        $this->goodsModel = $goodsModel;
        $this->goodsAdminModel = $goodsAdminModel;
        $this->authStorage = $authStorage;
        $this->goodsForm = $goodsForm;
        $this->fm = $this->plugin(FlashMessenger::class);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function productsAction()
    {
        $this->layout()->setVariable('active', 'goods');
        $this->setViewModelsList([
            'paginator',
            'productTimeFormat',
            'defaultPhoto',
            'idActiveSpan',
            'orderSorting',
            'columnSorting',
            'pathPhoto'
        ]);

        $this->setPathPhoto(ImagesModel::UPLOAD_IMAGE_PATH_VIEW . '/' . ImagesModel::IMAGE_PATH_SMALL . '/');
        $this->setProductTimeFormat(GoodsModel::PRODUCT_DATA_TIME_FORMAT);
        $this->setDefaultPhoto(GoodsModel::PRODUCT_PATH_DEFAULT_SMALL_PHOTO);
        $this->setOrderSorting(GoodsModel::DEFAULT_ORDER_SELECT);
        $this->setColumnSorting(GoodsModel::DEFAULT_COLUMN_SELECT);
        $this->setIdActiveSpan($this->goodsModel->getIdActiveSpanForProducts(
            $this->getColumnSorting(), $this->getOrderSorting()
        ));

        $paginator = $this->goodsModel->getListByParameters(
            null,
            $this->getColumnSorting(),
            $this->getOrderSorting()
        );
        $paginator->setCurrentPageNumber((int) $this->params('id', 1));
        $paginator->setItemCountPerPage(GoodsModel::PRODUCT_GOODS_PAGINATOR);
        $this->setPaginator($paginator);

        return $this->getViewModel();
    }

    /**
     * @return \Zend\Http\Response|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function productAction()
    {
        $this->setViewModelsList([
            'goodsForm',
            'pathPhoto',
            'fm'
        ]);

        $request = $this->getRequest();
        if ($request->isPost()) {

            $this->setGoodsForm($this->getGoodsForm()->setData($request->getPost()));

            if (!$this->getGoodsForm()->isValid()) {
                return $this->getViewModel();
            }

            $goods = new Goods((array) $request->getPost());
            if ('' == $request->getPost('photo', '')) {
                $imagesModel = new ImagesModel();
                $goods->setPhoto($imagesModel->upload());
            }

            if ($this->goodsModel->update($goods)) {
                $this->fm->addMessage(GoodsAdminModel::MESSAGE_SUCCESS_EDIT_GOODS);
                return $this->redirect()->toRoute('admin-goods', ['action' => 'product', 'id' => $request->getPost('id')]);
            }
            return $this->redirect()->toRoute('admin-goods', ['action' => 'products']);
        }

        if (!$idProduct = $this->params('id', null)) {
            return $this->redirect()->toRoute('admin-goods', ['action' => 'products']);
        }

        if (!$goods = $this->goodsModel->getGoodsById($idProduct)) {
            return $this->redirect()->toRoute('admin-goods', ['action' => 'products']);
        }

        $product = $this->goodsModel->getGoodsById($idProduct);
        $product = $this->goodsModel->convertPrices($product);
        $form = $this->getGoodsForm();
        if ($product->getPhoto()) {
            $this->setPathPhoto(ImagesModel::UPLOAD_IMAGE_PATH_VIEW . '/' . ImagesModel::IMAGE_PATH_MEDIUM . '/' . $product->getPhoto());
        }
        $this->setGoodsForm($form->setData($product->getArrayCopy()));

        return $this->getViewModel();
    }

    /**
     * @return \Zend\Http\Response|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function createProductAction()
    {
        $this->setViewModelsList([
            'goodsForm'
        ]);
        $request = $this->getRequest();
        if ($request->isPost()) {

            $this->setGoodsForm($this->getGoodsForm()->setData($request->getPost()));

            if (!$this->getGoodsForm()->isValid()) {
                return $this->getViewModel();
            }
            $imagesModel = new ImagesModel();
            $goods = new Goods($request->getPost()->toArray());
            $goods->setPhoto($imagesModel->upload());

            if ($newId = $this->goodsModel->save($goods)) {
                return $this->redirect()->toRoute('admin-goods', ['action' => 'product', 'id' => $newId]);
            }
            return $this->redirect()->toRoute('admin-goods', ['action' => 'products']);
        }

        return $this->getViewModel();
    }

    /**
     * @return \Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function selectAction()
    {
        $this->setViewModelsList([
            'paginator',
            'productTimeFormat',
            'defaultPhoto',
            'idActiveSpan',
            'orderSorting',
            'columnSorting',
            'pathPhoto'
        ]);
        $request = $this->getRequest();

        $this->setPathPhoto(ImagesModel::UPLOAD_IMAGE_PATH_VIEW . '/' . ImagesModel::IMAGE_PATH_SMALL . '/');
        $this->setProductTimeFormat(GoodsModel::PRODUCT_DATA_TIME_FORMAT);
        $this->setDefaultPhoto(GoodsModel::PRODUCT_PATH_DEFAULT_SMALL_PHOTO);
        $this->setOrderSorting($request->getPost('order', GoodsModel::DEFAULT_ORDER_SELECT));
        $this->setColumnSorting($request->getPost('column', GoodsModel::DEFAULT_COLUMN_SELECT));
        $this->setIdActiveSpan($this->goodsModel->getIdActiveSpanForProducts(
            $this->getColumnSorting(), $this->getOrderSorting()
        ));

        $paginator = $this->goodsModel->getListByParameters(
            $request->getPost('text', null),
            $this->getColumnSorting(),
            $this->getOrderSorting()
        );
        $paginator->setCurrentPageNumber((int) $request->getPost('page', 1));
        $paginator->setItemCountPerPage(GoodsModel::PRODUCT_GOODS_PAGINATOR);
        $this->setPaginator($paginator);

        return $this->getViewModel(false);
    }

    /**
     * @return \Zend\Http\Response|\Zend\View\Model\ViewModel
     * @throws ImageException
     * @throws \Exception
     */
    public function  editImageAction()
    {
        $this->setViewModelsList([
            'pathPhoto',
            'form',
            'idProduct',
            'fm'
        ]);

        if (!$idProduct = $this->params('id', null)) {
            return $this->redirect()->toRoute('admin-goods', ['action' => 'products']);
        }

        $product = $this->goodsModel->getGoodsById($idProduct);
        $request = $this->getRequest();
        if ($request->isPost()) {

            $imagesModel = new ImagesModel();
            if (!$imagesModel->resizeImage($product->getPhoto(), $request->getPost()->toArray())) {
                throw new ImageException('Error resize image');
            }
            $this->fm->addMessage(ImagesModel::MESSAGE_SUCCESS_EDIT_IMAGE);
            return $this->redirect()->toRoute('admin-goods', ['action' => 'product', 'id' => $idProduct]);
        }

        $this->setIdProduct($idProduct);
        $form = new ImageResizeForm();
        $this->setForm($form);

        if ($product->getPhoto()) {
            $this->setPathPhoto(ImagesModel::UPLOAD_IMAGE_PATH_VIEW . '/' . ImagesModel::IMAGE_PATH_DEFAULT . '/' . $product->getPhoto());
        }

        return $this->getViewModel();
    }

    /**
     * @param $paginator
     */
    protected function setPaginator($paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * @return mixed
     */
    protected function getPaginator()
    {
        return $this->paginator;
    }

    /**
     * @param $productTimeFormat
     */
    protected function setProductTimeFormat($productTimeFormat)
    {
        $this->productTimeFormat = $productTimeFormat;
    }

    /**
     * @return string
     */
    protected function getProductTimeFormat()
    {
        return $this->productTimeFormat;
    }

    /**
     * @param $defaultPhoto
     */
    protected function setDefaultPhoto($defaultPhoto)
    {
        $this->defaultPhoto = $defaultPhoto;
    }

    /**
     * @return string
     */
    protected function getDefaultPhoto()
    {
        return $this->defaultPhoto;
    }

    /**
     * @param $orderSorting
     */
    protected function setOrderSorting($orderSorting)
    {
        $this->orderSorting = $orderSorting;
    }

    /**
     * @return string
     */
    protected function getOrderSorting()
    {
        return $this->orderSorting;
    }

    /**
     * @param $columnSorting
     */
    protected function setColumnSorting($columnSorting)
    {
        $this->columnSorting = $columnSorting;
    }

    /**
     * @return string
     */
    protected function getColumnSorting()
    {
        return $this->columnSorting;
    }

    /**
     * @param $idActiveSpan
     */
    protected function setIdActiveSpan($idActiveSpan)
    {
        $this->idActiveSpan = $idActiveSpan;
    }

    /**
     * @return string
     */
    protected function getIdActiveSpan()
    {
        return $this->idActiveSpan;
    }

    /**
     * @param $goodsForm
     */
    protected function setGoodsForm($goodsForm)
    {
        $this->goodsForm = $goodsForm;
    }

    /**
     * @return object
     */
    protected function getGoodsForm()
    {
        return $this->goodsForm;
    }

    /**
     * @param $pathPhoto
     */
    protected function setPathPhoto($pathPhoto)
    {
        $this->pathPhoto = $pathPhoto;
    }

    /**
     * @return string
     */
    protected function getPathPhoto()
    {
        return $this->pathPhoto;
    }

    /**
     * @param $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return object
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param $idProduct
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;
    }

    /**
     * @return int
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * @return mixed|object
     */
    public function getFm()
    {
        return $this->fm;
    }
}