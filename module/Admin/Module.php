<?php


namespace Admin;

use Admin\Form\GoodsForm;
use Admin\Model\GoodsAdminModel;
use Admin\Model\TypeAdminModel;
use Admin\Model\UserAdminModel;
use Goods\Model\GoodsModel;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;


class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                GoodsAdminModel::class => function($container) {
                    $em = $container->get('doctrine.entitymanager.orm_default');
                    $authStorage = $container->get('Auth_storage');
                    return new Model\GoodsAdminModel($em, $authStorage);
                },

                TypeAdminModel::class => function($container) {
                    $em = $container->get('doctrine.entitymanager.orm_default');
                    return new Model\TypeAdminModel($em);
                },

                UserAdminModel::class => function($container) {
                    $em = $container->get('doctrine.entitymanager.orm_default');
                    return new Model\UserAdminModel($em);
                },

                GoodsForm::class => function($container) {
                    $goodsModel = $container->get(GoodsModel::class);
                    return new GoodsForm($goodsModel);
                },
            ],
        ];
    }

}