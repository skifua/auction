<?php


namespace Goods\Model;


use Admin\Exception\ImageException;
use Admin\Model\ImagesModel;
use Goods\Entity\Goods;
use Goods\Entity\TypeGoods;
use Users\Model\UserModel;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;

class GoodsModel
{

    const PRODUCT_GOODS_PAGINATOR = 6;
    const PRODUCT_DATA_TIME_FORMAT = 'd-M-Y H:i:s';
    const PRODUCT_PATH_DEFAULT_PHOTO = '/img/images/no_image/no_image.png';
    const PRODUCT_PATH_DEFAULT_SMALL_PHOTO = '/img/images/no_image/small_no_image.png';
    const DEFAULT_ORDER_SELECT = 'ASC';
    const DEFAULT_COLUMN_SELECT = 'name';
    const INFOJS_BUYER_FALSE = 'Sorry but a bid not been made';
    const ERRORJS_PRODUCT_FALSE = 'Sorry but a Lot can not be found';


    /**
     * @var object EntityManager
     */
    protected $em;

    /**
     * @var int User Id
     */
    public $authorId;


    public function __construct($em, $authStorage)
    {
        $this->em = $em;
        $this->authorId = $authStorage->read()[UserModel::AUTH_NAME_STAGING_USER_ID];
    }

    /**
     * @return mixed
     */
    public function fetchAll()
    {
        $result = $this->em->getRepository(Goods::class)
                        ->findAll();
        $paginator = new Paginator(new ArrayAdapter($result));

        return $paginator;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getGoodsById($id)
    {
        $result = $this->em->getRepository(Goods::class)
            ->find($id);
        return $result;
    }

    /**
     * @param null $text
     * @param string $column
     * @param string $order
     * @return Paginator
     */
    public function getListByParameters(
        $text=null,
        $column=self::DEFAULT_COLUMN_SELECT,
        $order=self::DEFAULT_ORDER_SELECT)
    {
        $nik = 'g';
        $query = $this->em->createQueryBuilder()
            ->select($nik)
            ->from(Goods::class, $nik)
            ->where($nik . '.name LIKE :text')
            ->setParameter('text', '%' . $text . '%')
            ->orderBy($nik . '.' . $column, $order)
            ->getQuery()
            ->getResult();

        $paginator = new Paginator(new ArrayAdapter($query));
        return $paginator;
    }

    /**
     * @param $column
     * @param $order
     * @return string
     */
    public function getIdActiveSpanForProducts($column, $order)
    {
       return $column . ucfirst(strtolower($order));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function makeBetById($id, $pagePrice) {
        $result = $this->em->getRepository(Goods::class)
            ->find($id);

        if (!$result) {
            return null;
        }

        if (!$result->getDynamic() || $result->getBuyer() == $this->authorId) {
            return $result;
        }

        if ($result->getPrice() != $pagePrice) {
            return $result;
        }

        $newPrice = $result->getStepPrice() + $result->getPrice();
        $result->setPrice($newPrice);
        $result->setBuyer($this->authorId);
        $this->em->flush();

        return $result;
    }

    /**
     * @param Goods
     * @return bool
     */
    public function checkBuyer($product)
    {
        if (!$product->getBuyer() || $product->getBuyer() != $this->authorId) {
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function getListTypesGoods()
    {
        $result = [];
        $allData = $this->em->getRepository(TypeGoods::class)
            ->findAll();
        $key = '0';
        foreach ($allData as $row) {
            $result [$row->getId()] = $row->getName();
            $key++;
        }

        return $result;
    }

    /**
     * @param $goods
     * @return mixed
     */
    public function save($goods)
    {
        $this->em->persist($goods);
        $this->adapterGoods($goods);
        $this->em->flush();

        return $goods->getId();
    }

    /**
     * @param $goods
     * @return bool
     */
    public function update($goods)
    {
        $goodsDb = $this->em->getRepository(Goods::class)
            ->find($goods->getId());
        if (!$goodsDb) {
            return false;
        }

        if ($goodsDb->getPhoto() && $goods->getPhoto() != $goodsDb->getPhoto())
        {
            if (!ImagesModel::deleteImages($goodsDb->getPhoto())) {
                throw new ImageException ('Error delete images');
            }
        }

        $goodsDb->setOptions($goods->getArrayCopy());
        $this->adapterGoods($goodsDb);

        $this->em->flush();

        return true;
    }

    /**
     * @param object Goods
     * @return mixed
     */
    public function adapterGoods($goods)
    {
        $typeId = $this->em->getRepository(TypeGoods::class)
            ->find($goods->getTypeId());

        $dateStart = new \DateTime( $goods->getDateStart());
        $dateStop = new \DateTime( $goods->getDateStop());
        $dynamic = $goods->getDynamic();
        $price = (int) $goods->getPrice() * 100;
        $stepPrice = (int) $goods->getStepPrice() * 100;
        $startPrice = (int) $goods->getStartPrice() * 100;

        if (0 == $price) {
            $price = $startPrice;
        }

        if ($dynamic != 1) {
            $goods->setDynamic(null);
        }

        $goods
            ->setTypeId($typeId)
            ->setDateStart($dateStart)
            ->setDateStop($dateStop)
            ->setPrice($price)
            ->setStartPrice($startPrice)
            ->setStepPrice($stepPrice);

        $this->em->persist($goods);
        return ($goods);
    }

    /**
     * @param $goods
     * @return mixed
     */
    public function convertPrices($goods)
    {
        $price = $goods->getPriceDecimal();
        $stepPrice = $goods->getStepPriceDecimal();
        $startPrice = $goods->getStartPriceDecimal();

        $goods
            ->setPrice($price)
            ->setStartPrice($startPrice)
            ->setStepPrice($stepPrice);
        return $goods;
    }
}