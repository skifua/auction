<?php


namespace Goods\Controller;


use Admin\Model\ImagesModel;
use Application\Controller\TemplateController;
use Goods\Model\GoodsModel;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;

class GoodsController extends TemplateController
{
    /**
     * @var object EntityManager
     */
    protected $em;

    /**
     * @var object GoodsModel
     */
    protected $goodsModel;

    /**
     * @var object AuthStorage
     */
    protected $authStorage;

    /**
     * @var object Paginator
     */
    protected $paginator;

    /**
     * @var object Goods
     */
    protected $product;

    /**
     * @var string
     */
    protected $productTimeFormat;

    /**
     * @var string
     */
    protected $defaultPhoto;

    /**
     * @var string
     */
    protected $idActiveSpan;

    /**
     * @var string
     */
    protected $orderSorting;

    /**
     * @var string
     */
    protected $columnSorting;

    /**
     * @var string
     */
    protected $pathPhoto;


    public function __construct(
        $em,
        $goodsModel,
        $authStorage)
    {
        $this->fm = $this->plugin(FlashMessenger::class);
        $this->em = $em;
        $this->goodsModel = $goodsModel;
        $this->authStorage = $authStorage;
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function index()
    {
        return $this->getViewModel();
    }

    /**
     * @return \Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function productsAction()
    {
        $this->layout()->setVariable('active', 'products');
        $this->setViewModelsList([
            'paginator',
            'productTimeFormat',
            'defaultPhoto',
            'idActiveSpan',
            'orderSorting',
            'columnSorting',
            'pathPhoto'
        ]);

        $this->setPathPhoto(ImagesModel::UPLOAD_IMAGE_PATH_VIEW . '/' . ImagesModel::IMAGE_PATH_SMALL . '/');
        $this->setProductTimeFormat(GoodsModel::PRODUCT_DATA_TIME_FORMAT);
        $this->setDefaultPhoto(GoodsModel::PRODUCT_PATH_DEFAULT_SMALL_PHOTO);
        $this->setOrderSorting(GoodsModel::DEFAULT_ORDER_SELECT);
        $this->setColumnSorting(GoodsModel::DEFAULT_COLUMN_SELECT);
        $this->setIdActiveSpan($this->goodsModel->getIdActiveSpanForProducts(
            $this->getColumnSorting(), $this->getOrderSorting()
        ));

        $paginator = $this->goodsModel->getListByParameters(
            null,
            $this->getColumnSorting(),
            $this->getOrderSorting()
        );
        $paginator->setCurrentPageNumber((int) $this->params('id', 1));
        $paginator->setItemCountPerPage(GoodsModel::PRODUCT_GOODS_PAGINATOR);
        $this->setPaginator($paginator);

        return $this->getViewModel();
    }

    /**
     * @return \Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function selectAction()
    {
        $this->setViewModelsList([
            'paginator',
            'productTimeFormat',
            'defaultPhoto',
            'idActiveSpan',
            'orderSorting',
            'columnSorting',
            'pathPhoto'
        ]);

        $request = $this->getRequest();

        $this->setPathPhoto(ImagesModel::UPLOAD_IMAGE_PATH_VIEW . '/' . ImagesModel::IMAGE_PATH_SMALL . '/');
        $this->setProductTimeFormat(GoodsModel::PRODUCT_DATA_TIME_FORMAT);
        $this->setDefaultPhoto(GoodsModel::PRODUCT_PATH_DEFAULT_SMALL_PHOTO);
        $this->setOrderSorting($request->getPost('order', GoodsModel::DEFAULT_ORDER_SELECT));
        $this->setColumnSorting($request->getPost('column', GoodsModel::DEFAULT_COLUMN_SELECT));
        $this->setIdActiveSpan($this->goodsModel->getIdActiveSpanForProducts(
            $this->getColumnSorting(), $this->getOrderSorting()
        ));

        $paginator = $this->goodsModel->getListByParameters(
            $request->getPost('text', null),
            $this->getColumnSorting(),
            $this->getOrderSorting()
        );
        $paginator->setCurrentPageNumber((int) $request->getPost('page', 1));
        $paginator->setItemCountPerPage(GoodsModel::PRODUCT_GOODS_PAGINATOR);
        $this->setPaginator($paginator);

        return $this->getViewModel(false);
    }

    /**
     * @return \Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function productAction()
    {
        $this->setViewModelsList([
            'product',
            'productTimeFormat',
            'defaultPhoto',
            'authorId',
            'pathPhoto'
        ]);

        $this->setProductTimeFormat(GoodsModel::PRODUCT_DATA_TIME_FORMAT);
        $this->setDefaultPhoto(GoodsModel::PRODUCT_PATH_DEFAULT_PHOTO);

        $product = $this->goodsModel->getGoodsById($this->params('id', 1));
        $this->setProduct($product);
        if ($product->getPhoto()) {
            $this->setPathPhoto(ImagesModel::UPLOAD_IMAGE_PATH_VIEW . '/' . ImagesModel::IMAGE_PATH_MEDIUM . '/' . $product->getPhoto());
        }

        return $this->getViewModel();
    }

    /**
     * @return mixed
     */
    public function checkPriceAction()
    {
        $request = $this->getRequest();
        if (!$product = $this->goodsModel->getGoodsById((int) $request->getPost('id'))) {
            $this->setErrorResponseJson('product', GoodsModel::INFOJS_BUYER_FALSE);
            return $this->responseJson();
        }

        $this->setDataResponseJson('price', $product->getPriceDecimal());
        $this->setDataResponseJson('dynamic', $product->getDynamic() ? true : false);
        $this->setDataResponseJson('buyer', $this->goodsModel->checkBuyer($product));

        return $this->responseJson();
    }

    /**
     * @return mixed
     */
    public function makeBetAction()
    {
        $request = $this->getRequest();

        if (!$product = $this->goodsModel->makeBetById((int) $request->getPost('id'), (int) $request->getPost('price'))) {
            $this->setErrorResponseJson('product', GoodsModel::ERRORJS_PRODUCT_FALSE);
            return $this->responseJson();
        }
        if (!$this->goodsModel->checkBuyer($product)) {
            $this->setInfoResponseJson('buyer', GoodsModel::INFOJS_BUYER_FALSE);
        }

        $this->setDataResponseJson('price', $product->getPriceDecimal());
        $this->setDataResponseJson('dynamic', $product->getDynamic() ? true : false);
        $this->setDataResponseJson('buyer', $this->goodsModel->checkBuyer($product));

        return $this->responseJson();
    }


    /**
     * @param $paginator
     */
    protected function setPaginator($paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * @return mixed
     */
    protected function getPaginator()
    {
        return $this->paginator;
    }

    /**
     * @param $product
     */
    protected function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return object
     */
    protected function getProduct()
    {
        return $this->product;
    }

    /**
     * @param $productTimeFormat
     */
    protected function setProductTimeFormat($productTimeFormat)
    {
        $this->productTimeFormat = $productTimeFormat;
    }

    /**
     * @return string
     */
    protected function getProductTimeFormat()
    {
        return $this->productTimeFormat;
    }

    /**
     * @param $defaultPhoto
     */
    protected function setDefaultPhoto($defaultPhoto)
    {
        $this->defaultPhoto = $defaultPhoto;
    }

    /**
     * @return string
     */
    protected function getDefaultPhoto()
    {
        return $this->defaultPhoto;
    }

    /**
     * @param $idActiveSpan
     */
    protected function setIdActiveSpan($idActiveSpan)
    {
        $this->idActiveSpan = $idActiveSpan;
    }

    /**
     * @return string
     */
    protected function getIdActiveSpan()
    {
        return $this->idActiveSpan;
    }

    /**
     * @param $orderSorting
     */
    protected function setOrderSorting($orderSorting)
    {
        $this->orderSorting = $orderSorting;
    }

    /**
     * @return string
     */
    protected function getOrderSorting()
    {
        return $this->orderSorting;
    }

    /**
     * @param $columnSorting
     */
    protected function setColumnSorting($columnSorting)
    {
        $this->columnSorting = $columnSorting;
    }

    /**
     * @return string
     */
    protected function getColumnSorting()
    {
        return $this->columnSorting;
    }

    /**
     * @return int
     */
    protected function getAuthorId()
    {
        return $this->goodsModel->authorId;
    }

    /**
     * @param $pathPhoto
     */
    protected function setPathPhoto($pathPhoto)
    {
        $this->pathPhoto = $pathPhoto;
    }

    /**
     * @return string
     */
    protected function getPathPhoto()
    {
        return $this->pathPhoto;
    }

}