
$(document).ready(function () {

    $('#originImage').imgAreaSelect({
        aspectRatio: '7:4',
        handles: true,
        show: true,
        onSelectEnd: function ( image, selection ) {
            $('input[name=x1]').val(selection.x1);
            $('input[name=y1]').val(selection.y1);
            $('input[name=w]').val(selection.width);
            $('input[name=h]').val(selection.height);
        }
    });
});


