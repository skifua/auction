var HTML_FOR_IMAGE_FORM = '<div class="form-group"><input type="file" name="file" id="file"></div>';

$(document).ready(function() {
    $('#deleteImage').on('click', function() {
        $('#imageInForm').html(HTML_FOR_IMAGE_FORM);
        $('input[name="photo"]').val('');
    })
});
